<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "proximity feature" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="boolean" Label="Boolean" BaseType="operation">

      <BriefDescription>Perform a set-based boolean operation.</BriefDescription>
      <DetailedDescription>
        Given side sets as workpieces (to be modified) and tools (used to modify
        the workpiece), choose a subset of all the primitives in the side sets
        as the output. This does not perform any geometric processing; membership
        in the output is solely based on the chosen operation (union, intersection,
        difference) and the global IDs assigned to the primitives.

        For union operations, the output workpiece will contain every primitive
        that appears in either the input workpiece or the tools.

        For intersection operations, the output workpiece will contain only primitives
        that appear in both the input workpiece and the (union of the) tools.

        For difference operations, the output workpiece will contain only primitives
        that appear in *only* the workpiece (and not any tool).

        For symmetric difference operations, the workpiece will contain primitives that
        appear in the exclusive-or of the (union of the) workpieces and the (union of the) tools.
        Put another way, the symmetric difference is the union of the workpiece minute the tool
        and the tool minus the workpiece.
      </DetailedDescription>
      <AssociationsDef Name="workpieces" NumberOfRequiredValues="1" Extensible="true">
        <BriefDescription>Side sets to be modified.</BriefDescription>
          <!-- TODO: Accepts should include only side sets -->
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="any"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Int Name="operation" NumberOfRequiredValues="1">
          <BriefDescription>What type of Boolean operation should be performed?</BriefDescription>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="union">0</Value>
            <Value Enum="intersection">1</Value>
            <Value Enum="difference">2</Value>
            <Value Enum="symmetric difference">3</Value>
          </DiscreteInfo>
        </Int>

        <Component Name="tools" NumberOfRequiredValues="0" Extensible="true">
          <BriefDescription>Geometry that will act on the workpiece.</BriefDescription>
          <!-- TODO: Accepts should include only side sets? Maybe allow tools to be primaries? -->
          <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="any"/></Accepts>
        </Component>

        <Void Name="keep inputs" Optional="true" IsEnabledByDefault="false">
          <BriefDescription>Should the workpiece and tool objects survive the operation?</BriefDescription>
        </Void>

      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(boolean)" BaseType="result">
      <ItemDefinitions>
        <Void Name="allow camera reset" Optional="true" IsEnabledByDefault="true" AdvanceLevel="11"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
