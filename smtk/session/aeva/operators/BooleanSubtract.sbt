<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "proximity feature" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="boolean subtract" Label="Union" BaseType="operation">

      <BriefDescription>Perform a set-based boolean subtraction.</BriefDescription>
      <DetailedDescription>
        The output workpiece will contain every primitive
        that appears in only the input workpiece but not the tools.
      </DetailedDescription>
      <AssociationsDef Name="workpieces" Label="workpiece" NumberOfRequiredValues="1" Extensible="false">
        <BriefDescription>Side sets to be subtracted.</BriefDescription>
          <!-- TODO: Accepts should include only side sets -->
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="any"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Component Name="tools" Label="tool" NumberOfRequiredValues="1" Extensible="false">
          <BriefDescription>Geometry that will act on the workpiece.</BriefDescription>
          <!-- TODO: Accepts should include only side sets? Maybe allow tools to be primaries? -->
          <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="any"/></Accepts>
        </Component>

        <Void Name="keep inputs" Optional="true" IsEnabledByDefault="true">
          <BriefDescription>Should the workpiece and tool objects survive the operation?</BriefDescription>
        </Void>

      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(boolean subtract)" BaseType="result">
      <ItemDefinitions>
        <Void Name="allow camera reset" Optional="true" IsEnabledByDefault="true" AdvanceLevel="11"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
    <View
      Type="Operation"
      Title="Subtract"
      TopLevel="true"
      FilterByAdvanceLevel="false"
      FilterByCategoryMode="false"
      >
    <InstancedAttributes>
      <Att Type="boolean subtract" Name="boolean subtract">
      <ItemViews>
        <View Item="tools" Type="qtReferenceItem"/>
      </ItemViews>
    </Att>
  </InstancedAttributes>
</View>
  </Views>
</SMTK_AttributeResource>
