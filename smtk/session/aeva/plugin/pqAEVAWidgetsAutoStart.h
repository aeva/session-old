//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_pqAEVAWidgetsAutoStart_h
#define smtk_session_aeva_pqAEVAWidgetsAutoStart_h

#include <QObject>

class vtkSMProxy;

class pqAEVAWidgetsAutoStart : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  pqAEVAWidgetsAutoStart(QObject* parent = nullptr);
  ~pqAEVAWidgetsAutoStart() override;

  void startup();
  void shutdown();

private:
  Q_DISABLE_COPY(pqAEVAWidgetsAutoStart);
};

#endif
