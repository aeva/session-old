cmake_minimum_required(VERSION 3.12)

# In order to use a new superbuild, please copy the item from the date stamped
# directory into the `keep` directory. This ensure that new versions will not
# be removed by the uploader script and preserve them for debugging in the
# future.

set(data_host "https://data.kitware.com")

# Determine the tarball to download.
if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "vs2019")
  # 20200913
  # When paraview includes https://gitlab.kitware.com/vtk/vtk/-/merge_requests/7191,
  # remove path fix from os-windows.yml
  set(file_item "5f612bbd50a41e3d19896b37")
  set(file_hash "299d1701482e0c52d4bce30be10ac31c75bebfad37b1df990d1f076a832ba47021604f2f3d2e495967b6951ff48c7ec18c78f5cd6a38a424c1d7719a9b7dbc3d")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "macos")
  # 20200910
  set(file_item "5f5a227e50a41e3d19793102")
  set(file_hash "7aafa9ce438ad7efd9690ff05d5a68b2d93165aed12349f184c907e0ff9a927ce0342ccf97ea45b9c2ebb36563becd0c87ded8b445d09357420b2144cfcaf4c9")
else ()
  message(FATAL_ERROR
    "Unknown build to use for the superbuild")
endif ()

# Ensure we have a hash to verify.
if (NOT DEFINED file_item OR NOT DEFINED file_hash)
  message(FATAL_ERROR
    "Unknown file and hash for the superbuild")
endif ()

# Download the file.
file(DOWNLOAD
  "${data_host}/api/v1/item/${file_item}/download"
  ".gitlab/superbuild.tar.gz"
  STATUS download_status
  EXPECTED_HASH "SHA512=${file_hash}")

# Check the download status.
list(GET download_status 0 res)
if (res)
  list(GET download_status 1 err)
  message(FATAL_ERROR
    "Failed to download superbuild.tar.gz: ${err}")
endif ()

# Extract the file.
execute_process(
  COMMAND
    "${CMAKE_COMMAND}"
    -E tar
    xf ".gitlab/superbuild.tar.gz"
  RESULT_VARIABLE res
  ERROR_VARIABLE err
  ERROR_STRIP_TRAILING_WHITESPACE)
if (res)
  message(FATAL_ERROR
    "Failed to extract superbuild.tar.gz: ${err}")
endif ()
